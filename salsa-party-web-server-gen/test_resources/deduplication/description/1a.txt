VIDA NIGHT | FREITAG 27.08.2021
★ VIDA PARTY ★
SEKTOR11 TANZCLUB in Zürich
★ PARTY OHNE MASKEN ★
★ COVID-19 TEST und ZERTIFIKAT VOR ORT ★
★ 2 INDOOR & 1 OUTDOOR FLOORS ★
★★★ GOOD NEWS ★★★
GRATIS COVID-19 TEST und ZERTIFIKAT VOR ORT IM SEKTOR11 mit der APOTHEKE GLATTPARK https://www.facebook.com/TopPharm
Die Apotheke Glattpark in Glattbrugg wird vor Ort im Sektor11 vor der SALSAVIDA Party dabei sein und die COVID-19 Tests in kürzester Zeit ausführen und das gültige Zertifikat erstellen. Alles VOR ORT! WICHTIG eine gültige Krankenkasse Ausweis und ID /Ausweis mitnehmen. Der Test ist KOSTENLOS. Du kannst den Prozess sogar noch beschleunigen und die Wartezeit verringern, indem Du Dich gleich für den Test vorab per E-Mail anmeldest. Damit entfällt das Ausfüllen des Formulars vor Ort und du kannst gleich testen. Dann ist alles für dich bereit, wenn du ankommst.
Wie funktionierts:
1. Sende bitte ein Foto Deines Krankenkassen Ausweises (Karte) per E-Mail an info@glattpark.apotheke.ch Apotheke Glattpark mit dem folgenden Betreff: Sektor11 Salsavida Party
2. Vor dem Eingang dein ID-Ausweis vorweisen und gleich mit dem Test starten
ODER
3. Ohne Voranmeldung vor dem Eingang Deinen Krankenkasse Ausweis UND ID-Ausweis vorweisen und das Formular ausfüllen. Danach mit dem Test starten.
4. Nach ca. 10 Min. Testresultat und Zertifikat. Das Zertifikat ist 48 Std. gültig und Du kannst es das ganze Wochenende an jede Party mitnehmen.
5. Danach kanns losgehen mit Feiern!
Test und Zertifikat sind für Dich Kostenlos!
WICHTIG BITTE NICHT VERGESSEN!!! KRANKENKASSE AUSWEIS (KARTE) UND ID-AUSWEIS MITNEHMEN!
=== ★★★ SALSAVIDA ★★★ ===
Das Tanzparket ist wieder eröffnet und zusammen mit dir feiern wir die Salsavida Friday VIDA Night am Freitag, 27. August 2021 ab 21:30h in der exzellenten Location SEKTOR11 ex OXA an der Andreasstrasse 70 in Zürich mit grosszügigen Dancefloors, Terrasse und Fiestaaa!
Wir freuen uns riesig Euch alle wieder auf dem Tanzparket zu treffen und mit Euch zu feiern!
Wir freuen uns auf DICH!
Francesco & SALSAVIDA-Team
SALSAVIDA aqui se bailaaa!
★★★ LET'S DANCE TOGETHER ★★★
DOOR OPEN: 21:30-03:00h
========================
★ CLUB Floor
Salsa Cubana, Timba, Son, Bachata
DJ Manuel / DJ Yasser
Entertainment by Damián Vergés Cuba
★ PRIVATE Floor
Bachata, Salsa All-Style
DJ Giovanni
★ Salsa Open Air Terrasse bei schönem Wetter
Salsa Cubana, Bachata
DJ Yasser
========================
Eintritt inkl. Garderobe CHF 15.-
Parking: Blaue Zone rund um die Location und viele Parkplätze im sehr nahen Parkhäuser
Parkhaus Messe, Hagenholzstrasse 50, 8050 Zürich https://goo.gl/maps/FMK6zZHrry2UcjkYA
Parkhaus Kunsteisbahn Oerlikon, Siewerdtstrasse 80, 8050 Zürich
https://goo.gl/maps/GVHSvuhF3wYLj58c9
ÖV: Sehr gut erreichbar mit Tram und Bahn
Location Map: https://g.page/sektor11?share
SEKTOR 11
Andreasstrasse 70
8050 Zürich-Örlikon
Wir freuen uns auf DICH!
SALSAVIDA aqui se bailaaa!
