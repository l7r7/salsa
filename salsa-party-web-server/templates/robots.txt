Sitemap: @{SitemapR}

User-agent: SemrushBot
Disallow: /

User-agent: PetalBot
Disallow: /

User-agent: AhrefsBot
Disallow: /

User-agent: SeznamBot
Disallow: /

User-agent: *
Allow: /
